const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string')
  {
    return false;
  }

  if (str1.length === 0 && str2.length === 0)
  {
    return '0';
  }

  for (let character of str1)
  {
    if (isNaN(parseInt(character)))
    {
      return false;
    }
  }

  for (let character of str2)
  {
    if (isNaN(parseInt(character)))
    {
      return false;
    }
  }

  let number1 = str1;
  let number2 = str2;

  if (str1.length === 0)
  {
    return str2;
  }
  else if (str2.length === 0)
  {
    return str1;
  }

  number1 = number1.split("").reverse();
  number2 = number2.split("").reverse();
  let result_array = [];
  let rest = 0;
  let for_add = 0;
  let less_number = str1.length >= str2.length ? number2 : number1;
  let max_number = str1.length >= str2.length ? number1 : number2;

  for (let i = 0; i < less_number.length; i++)
  {
    let addition_result = (parseInt(less_number[i]) + parseInt(max_number[i])).toString();
    if (addition_result.length === 2)
    {
      for_add = parseInt(addition_result[1]) + rest;
      rest = parseInt(addition_result[1]);
    }
    else if (addition_result.length === 1)
    {
      for_add = parseInt(addition_result[0]) + rest;
      rest = 0;
    }

    result_array.push(for_add.toString());
  }

  if (rest !== 0 && Math.abs(str1.length - str2.length) === 0)
  {
    result_array.push(rest.toString());
  }
  else if (Math.abs(str1.length - str2.length) > 0)
  {
    result_array.push((rest + max_number[less_number.length]).toString());
    for (let i = less_number.length + 1; i < max_number.length; i++)
    {
      result_array.push(max_number[i]);
    }
  }

  return result_array.reverse().join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let i = 0; i < listOfPosts.length; i++)
  {
    if (listOfPosts[i].author === authorName)
    {
      posts++;
    }

    if (typeof listOfPosts[i].comments !== 'undefined')
    {
      for (let comment of listOfPosts[i].comments)
      {
        if (comment.author === authorName)
        {
          comments++;
        }
      }
    }
  }

  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let twenty_five = 0;
  let fifty = 0;
  for (let i = 0; i < people.length; i++)
  {
    if (people[i] === 25)
    {
      twenty_five++;
    }
    else if (people[i] === 50)
    {
      twenty_five--;
      fifty++;
    }
    else if (people[i] === 100)
    {
      twenty_five--;
      fifty--;
    }

    if (twenty_five < 0 || fifty < 0)
    {
      return 'NO';
    }
  }

  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
